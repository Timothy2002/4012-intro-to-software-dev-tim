import requests
import random

def new_pet():
#creates a new pet
    pet = {
        "hunger": 5,
        "happiness": 5
    }
    return pet

def init_actions():
    # actions for the user to use for the pet
    actions = ["stroke", "feed", "play", "feed eggs", "ask for help"]
    return actions

def trump():
    # gets random trump quotes about obama from the link when player types "feed eggs"
    response = requests.get("https://api.tronalddump.io/search/quote",
        params={"query": "obama" })
    data = response.json()
    n_quotes = len(data["_embedded"]["quotes"])
    random_n = random.randint(0,n_quotes)
    rand_quote = data["_embedded"]["quotes"][random_n]["value"]
    return rand_quote

def fetch_advice():
    # gets a random piece of advice when player types "ask for advice" from the link
    advice = requests.get("https://api.adviceslip.com/advice").json()
    text = advice["slip"]["advice"]
    return text

def feed_pet(data):
    # feeding action
    pet["hunger"] += 1
    # if the hunger level gets to 0 or less the happiness level gose down after each feed
    if pet["hunger"] <= 0:
        pet["happiness"] -= 1
    return pet

def play_pet(data):
    # play action
    # adds happiness and takes hunger
    pet["happiness"] += 1
    pet["hunger"] -= 1
    return pet

def pet_respond(data):
    # how the pet responds to the players actions
    print("Your pet says:")
    if pet["happiness"] >= 10:  # if the pets happiness level is 10 or above it will respond with "Woof woof! and wags its tail."
        print("Woof woof! and wags its tail.")
    elif pet["happiness"] <= 0: # if the pets happiness level is 0 or below it will respond with "Whining"
        print("Whining")
    else: # if no requierments are met then it just responds with "woof"
        print("Woof")
    return

# the main program routine
if __name__ == "__main__":


    pet = new_pet()
    actions = init_actions()

    play = True

    while play:
        # shows the user what actions they can perform
        print("Available actions:")
        for action in actions:
            print(action)

        action = input("What action would you like to perform?")

        # check they have entered a valid action
        if action in actions:
            # if they feed it hunger increases
            if action == "feed":
                pet = feed_pet(pet)
            # if stroked, the pet gets happier
            elif action == "stroke":
                pet["happiness"] += 1
            # if fed eggs, the pet responds with a trump quote
            elif action == "feed eggs":
                print(trump())
            # if ask for help, it gives the user advice
            elif action == "ask for help":
                print(fetch_advice())
            # if played with, the pet gets happier and hungrier
            elif action == "play":
                pet = play_pet(pet)
        else: # if typed in somethigns that is not one of the listed function then responds with message below
            print("You cannot {} your pet.".format(str(action)))


        # make the pet do something to give the user feedback
        pet_respond(pet)

        # checks if the user wants to keep on playing
        answer = input("Want to keep playing? (y/n)")
        if answer == "n":
            play = False
