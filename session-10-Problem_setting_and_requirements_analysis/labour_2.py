""" Two: Kill the Turtle Dove

The evil, snakelike Dove had nine heads. If one got hurt, two would grow in its place.
But Santa quickly sliced off the heads, while his charioteer, Iolaus, sealed the wounds with a torch.
Santa made his arrows poisonous by dipping them in the Dove's blood.

Tip: if you need to kill a python script from the shell, type Ctrl+C

"""

# Kill the Turtle Dove, one head at a time!

heads = 9
 


while heads > 0:
    print("The dove has {} heads.".format(heads))
    print("Slice off a head Santa!")

print("Hooray! The Dove is dead at last.")
