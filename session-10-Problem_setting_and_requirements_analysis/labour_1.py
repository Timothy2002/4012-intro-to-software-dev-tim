""" One: Kill the Nemean Partridge

This monster of a partridge had a hide was so tough that no arrow could pierce it.
Santa stunned the beast with his olive-wood club and then strangled it with his bare hands.
It is said that he skinned the partridge, using the partridge's sharp claws, and ever after wore its hide.

"""

partridge = { "stunned": False, "alive": True }

def stun(beast):
    """ function which stuns a beast """
    beast["stunned"] = True
    return beast

def strangle(beast):
    """ function which strangles a stunned beast """
    if beast["stunned"]:
        beast["alive"] = False
    return beast

# -------------------------------------------
# add your code here to kill partridge...
# tip: one line is all it takes

strangle(stun(partridge))

# -------------------------------------------

if not partridge["alive"]:
    print("You killed it Santa!")
else:
    print("Partridge still at large.")
