# the pets condition 
pet = {
    "hunger": 5,
    "happiness": 5
}

actions = ["stroke", "feed", "play", "ignore"]

play = True

while play:
    name = str( input("What is the name of your pet: ") )

    # shows the user what actions they can perform
    print("Available actions for your pet:")
    for action in actions:
        print(action)




    action = input("What would you like to do with your pet?")

    # checks if the user has entered a valid action
    if action in actions:

        # if they feed it the pets hunger increases 
        if action == "feed":
            pet["hunger"] += 1
            print("You feed your pet some dog food / +1 hunger")

            # if hunger gets to 0 or less the pets happiness level decrease 
            if pet["hunger"] <= 0:
                pet["happiness"] -= 1

        # if the pet is stroked the pet gets happier
        elif action == "stroke":
            pet["happiness"] += 1
            print("You stroke your pet till it is satisfied / +1 happiness")            

        # if the pet is played with the pet gets happier and hungrier
        elif action == "play":
            pet["happiness"] += 1
            pet["hunger"] -= 1
            print("You go outside and play fetch with your pet / +1 happiness -1 hunger")

        ## if the pet is ignord
        elif action == "ignore":
            pet["happiness"] -=1
            print("Your pet tries to get your attention but you ignore it / -1 happiness")

    else: # when user inputs an incorrect command
        print("You cannot {} with your pet.".format(str(action)))


    # the pets feed back to the user
    print("Your pet says:")

    # if happiness gets >= 10 the pets replyes with message below
    if pet["happiness"] >= 10:
        print("'Woof' 'woof!' and wags its tail.")
    elif pet["happiness"] <= 0: # if happiness gets <= 0 the pets replyes with message below
        print("'Whine' 'whine' the pet feels sad")
    else:
        print("'Woof!'")

    # check if the user wants to keep playing
    answer = input("Do you want to keep on playing with your pet? (y/n)")
    if answer == "n":
        play = False
