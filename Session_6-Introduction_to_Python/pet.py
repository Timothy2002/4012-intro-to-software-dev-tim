actions = ["feed", "stroke", "walk", "kick"]

pet = {
    "name": "little white",
    "age": 2,
    "is_alive": True,
    "health": 10 
}

while pet["is_alive"]:
    print("Here are the actions you can perform")
    for action in actions:
        print(action)
    user_action = input("what would you like to do to your pet " + pet["name"])
    # Responses to users actions
    if user_action == "feed":
        pet["health"] += 1
        print("little white eats the food")
    elif user_action == "stroke":
        print("little white wages his tale")
    elif user_action == "walk":
        pet["health"] += 1
        print("little white barks happily")
    elif user_action == "kick":
        pet["health"] -= 2
        print("Little white Cries")
    # checks to see if the pet still has health
    # if not it will set is_alive to False
    if pet["health"] <= 0:
        pet["is_alive"] = False

print("Sorry your pet has past away")






