""" 
    Activity 2: Don't Repeat Yourself

    Reduce the repetition in this WET testing script.

    The script tests my example function from task_1.py
"""

from Task_1 import is_numeric
test = ["foobar","123","12345","1.23",".23.2.3."]

for i in test:

    count = 0 

    print("Running Tests",count,"...")
    result = is_numeric(i)
    if result == False:
        print("PASS")
    else:
        print("FAIL")

"""
    print("Running test 1...")
    result = is_numeric("foobar")
    if result == False:
        print("PASS")
    else:
        print("FAIL")

    print("Running test 2...")
    result = is_numeric("12")
    if result == True:
        print("PASS")
    else:
        print("FAIL")

    print("Running test 3...")
    result = is_numeric("12345")
    if result == True:
        print("PASS")
    else:
        print("FAIL")

    print("Running test 4...")
    result = is_numeric("1.23")
    if result == True:
        print("PASS")
    else:
        print("FAIL")

    print("Running test 5...")
    result = is_numeric(".23.2.3.")
    if result == False:
        print("PASS")
    else:
        print("FAIL")
"""