"""
Activity 1: KISS

Make a function that takes a string as input and returns
a boolean indicating whether the input is numeric.
 
Example test cases:

Input       Exp. Output     Description
-------------------------------------------
"foobar"    False           Not numeric
"12"        True            Small int
"12345"     True            Large int
"1.23"      True            Decimal
".1.2.3."   False           Invalid decimal

Tips:
    - Make your solution as simple as possible.
    - Make use of built-in functions where possible.
"""

    
def is_numeric(result):
    return (result.isnumeric())
