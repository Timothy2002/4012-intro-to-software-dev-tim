
# Adjacency list
A = [ [1, 2, 3],
      [5, 7],
      [4, 6],
      [5, 7],
      [2, 5, 7],
      [1, 3, 4, 7],
      [2],
      [1, 3, 4, 5] ]

def al_connected(n1, n2):
    # this checks if the 2 given nodes in the adjecency list are connected or not
    # n1:first node
    # n2:second node

    if n1 in A[n2] or n2 in A[n1]:
        return "connected"
    else:
        return "not connected"

print(al_connected(0, 2))


# Adjacency matrix
M = [ [0,1,1,1,0,0,0,0],
 	  [1,0,0,0,0,1,0,1],
 	  [1,0,0,0,1,0,1,0],
 	  [1,0,0,0,0,1,0,1],
 	  [0,0,1,0,0,1,0,1],
 	  [0,1,0,1,1,0,0,1],
 	  [0,0,1,0,0,0,0,0], 
 	  [0,1,0,1,1,1,0,0] ]

def am_connected(n1, n2):
    # this checks if the 2 given nodes in the adjecency matrix are connected or not
    # n1:first node
    # n2:second node

    if M[n1][n2] == 1:
        return "connected"
    else:
        return "not connected"

print(am_connected(0, 1))
