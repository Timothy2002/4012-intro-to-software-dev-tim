import math 

def cal_tax(a ,b):
    # some code here
    return math.ceil((b/100) * a)

def test_cal_tax():
    assert cal_tax(10000, 20) == 2000
    assert cal_tax(10651, 25) == 2663
