def validate_user_input(n):
    """
    function checks whether the amount entered
    by a user, n, is a positive numeric value.
    input n: the gross salary (int or float)
    returns: bool
    """

    # REPLACE THIS FUNCTION WITH YOUR
    # ACTIVITY 2 CODE!
    return n >0
    return False

def calc_tax(a, b):
    """
    function takes an amount, a, and calculates
    the amount of tax which should be applied 
    at the rate, b, expressed as a percent.
    returns amount of tax rounded to nearest whole number.

    input a: original amount (int)
    input b: tax rate (int) 
    returns: int
    """
    tax = a * (b/100)
    return round(tax)

def get_tax_band(n):
    """
    function returns the tax band code
    for a gross salary, n.
    input n: the salary (int, float)
    returns: str
    """
    if n <= 12500:
        return "PA"
    elif n > 12500 and n <= 50000:
        return "BR"
    elif n > 50000 and n <= 150000:
        return "HR"
    elif n > 150000:
        return "AR"

def get_total_tax(n):
    """
    function calculates the total tax
    payable based on the gross salary, n.
    input n: gross salary (int)
    returns: int
    """
    total_tax = 0
    tax_band = get_tax_band(n)
    if tax_band == "BR":
        total_tax += calc_tax(n - 12500, 20)
    elif tax_band == "HR":
        total_tax += calc_tax(50000 - 12500, 20)
        total_tax += calc_tax(n - 50000, 40)
    elif tax_band == "AR":
        total_tax += calc_tax(50000 - 12500, 20)
        total_tax += calc_tax(150000 - 50000, 40)
        total_tax += calc_tax(n - 150000, 45)
    return total_tax

def tax_calculator(n):
    """
    function takes some user input, n,
    and tries to calculate the tax owed
    on that amount.
    if the input is invalid it returns
    false.
    """
    if validate_user_input(n):
        return get_total_tax(n)
    else:
        return False
