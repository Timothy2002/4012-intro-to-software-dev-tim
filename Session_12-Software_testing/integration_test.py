from tax_calculator import *

def test_tax_calculator():
    assert tax_calculator(123000) == 36700
    assert tax_calculator(123000.50) == 36700
    assert tax_calculator(123002) == 36701
    assert type(tax_calculator(-1230)) == bool 


