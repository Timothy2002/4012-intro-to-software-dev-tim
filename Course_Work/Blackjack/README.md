---
Link to Git Repo: https://gitlab.com/Timothy2002/4012-intro-to-software-dev-tim
Title: README file of COM4012 Introduction to Software Development Assignment
---
## Description of what the project is for
This project artefact is made up of two parts: the first part is dealing with data structures, algorithmic problem-solving and functional programming and the second deals with software testing. 

In the evaluation part of the project, I will need to document the process of this project in detail with a conclusion of the project that explains the overall evaluation of the work I have done. The way that I will format this documentation is up to me but needs to be relevant to this project. Referencing industry practices done in the computer science industry can help me to back up my statements and I will reference them in the propriety academic format. 

With this project of COM4012 Intro to software development, I am put into a scenario linking to the project artefact of COM4002 Intro to Tech Stack in creating a skeleton artefact for an online casino. This time the same people want me to create the game blackjack with their own custom rules of the game to be put onto their casino website. I do not need to implement any type of human interface on the game I just need to implement the core logic of the game and the rules set by the client. 	

Requirement analysis 
Data structures, algorithmic problem-solving, and functional programming
I am being asked to implement the blackjack game according to the rules set by the client. I may implement the game in either JavaScript or Python. I am assessed on my approach to algorithmic problem-solving, ability to write well-designed function-based code and ability to structure program data logically. I should aim to make my code as testable as possible. Meaning encapsulating units of functionality within reusable functions that can be independently tested.
When referring to any online sources I need to be very clear about which parts of the code have been borrowed from elsewhere. Also, in the implementation of the game, it should have the functionality of the rules set by the client and not some other version of the game.
It is recommended that I attempt to write all the parts of the program myself, following an incremental development process. Meaning implementing one increment of functionality at a time and refactoring the code before moving to the next increment. Following the incremental process, it will minimize risk and ensure the quality and reliability of the coding. 

Unit testing
In the second part of the project, I am to identify units of functionality in the application which are testable. I should then aim to design and implement one or more unit tests. Recommended that I use a testing framework such as pytest for this purpose. Writing the test case before implementing the functions helps to ensure that the coding is reliable. This is consistent with the Test Driven Development (TDD) approach to software development.
The test functions should be written in separate .py files from the main program functions. The test script will import the program functions to test them. Running the test script should generate some meaningful output in the console.

Project evaluation
In the evaluation part of the project, I will be documenting the processes of the project including any planning and research done. This part shows my knowledge, analytical thinking and academic skills. I should aim to justify my approaches to the project and justifications would be referred to as industry practices. In the evaluation, I will include reflections on the work, what I have learned in the process of this project, where I have succeeded and where can I improve upon next time.

The rules of the game
�	The game will allow 1 player to play against a machine.
�	The player starts the game with a pile of chips.
�	The player may choose how many chips to 'buy' at the start of the game.
�	The game of blackjack is played in rounds.
�	The player may choose to stop playing after each round, in which case they end the game with however many chips are in their pile.
�	At the start of each round, the player must place a bet.
�	The minimum a player can bet in a given round is 1 chip. The maximum is 50 or the value of their pile (whichever is less).
�	After placing their bet, the player is dealt 2 cards ranging from 1-11. 
o	If the player's cards add up to 21, the player automatically wins the round.
o	If the player's cards add up to 22 they automatically lose the round.
o	If the player's cards add up to <22, the player is given the option to draw another card. They may repeat this action while the sum of their cards remains <22.
�	Once the player has ceased to draw cards, the machine will draw 2 cards ranging from 1-11. 
o	If the computer's cards exceed 21, the player wins the round.
o	If the computer's cards equal 21, the player loses the round.
o	If the computer's cards add up to <17, it must draw a third card. If not, it must stay.
�	If both the computer's and the player's cards <21, the player with the highest total wins the round.
�	If both totals are equal, the game is a draw.
�	If the computer wins, the player loses their bet.
�	If the player wins, they receive back their bet plus some amount extra. It is up to you to decide how much extra.
�	If the player loses all the chips in their pile, the game ends.

## Instructions for how to develop, use and test the code
Code comments are used on all of the coding. I have also seperated section of the code to my decion on seperate statments.

To run the python file of Black_Jack.py simply select the start up green button or: 
- open the Black_Jack.py file
- hover over the project tab
- click 'start with debugging

To run the test for the pytest files:
- cd into the folder directory for the Test_py folder
- input "pytest (specific file of the python file you want to test)"


