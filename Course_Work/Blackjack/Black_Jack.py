import random

# New game set up
def new_game():

    # Stores chip, bet and card total data
    player = {
        "chip": {player_chip_input}, 
        "bet": {player_bet_input},
        "card_total": {player_card_total}
    }
    return player
    
    # stores the computers card total
    computer = {
        "card_total": {computer_card_total}
    }
    return computer

# """---------------------------------------------------------------------------------------------------------------------------"""

def init_actions():
    # Actions for the user option case and end round option 
    actions = ["draw", "stay"]
    return actions

# """---------------------------------------------------------------------------------------------------------------------------"""

# The main program routine
if __name__ == "__main__":

    actions = init_actions()
    
    # Start game message
    print ("Welcome to the game: Blackjack")
    print ("How many chips do you want to buy")

    while True:# This is where the player input their chips
        number = input("Player chips input: ")
        try:
            player_chip_input = int(number)
            if player_chip_input < 1:  # If not a positive int, print message and ask for input again
                print("Sorry, input must be a positive integer, try again")
                continue
            break
        except ValueError:
            print("That is not a number!")     
    # Print message ending the player chip total input
    print(f'you have bought {player_chip_input} chips')
    
# """---------------------------------------------------------------------------------------------------------------------------"""
   
    # Loop happens here if player continues to play
    play = True
    while play:

        # This is where the player places their bet for the round
        while True:# Player bet input
            number = input("New round, place your bet for this round: ")
            try:
                player_bet_input = int(number)
                if player_bet_input > player_chip_input:  # Checks if the player has inputed a number over their chip pile total
                    print("Sorry, you do not have that high amount of chips , try again") 
                    continue
                elif player_bet_input < 1:  # Checks if the player inputs a number under 1
                    print("Sorry, that is too low of an input of chips , try again")
                    continue
                break
            except ValueError:
                print("That is not a number!")     
            # Print message ending the player bet input
        print(f'You have bet {player_bet_input} chips this round')

# """---------------------------------------------------------------------------------------------------------------------------"""
     
        # Game Message
        print ("Dealer: 'shuffuling and handing out two cards to the player'")

        # Player Card draw
        a = random.randint(1,11)
        b = random.randint(1,11)
        player_card_total = a + b
        print (f'You have the card total of:{player_card_total}')

        # Storing the bet input for the player
        player = new_game()       

# """---------------------------------------------------------------------------------------------------------------------------"""

        # Checks the Win, Lose and Draw conditions for the player
        if player_card_total == 21: # Players win condition receive bet back plus 5 chips
            print("The winner is the player")           
            player_bet_input += 5
            player_chip_input += player_bet_input
            print(f'Your bet +5 chips are added to your pile')
            print(f'Your total chip pile {player_chip_input}')

        elif player_card_total == 22: # Player lose condition lose their bet taking away their bet from their chip pile
            print("The winner is the computer")
            player_chip_input -= player_bet_input
            print(f'You lost your betting chips from your pile')
            print(f'Your total chip pile {player_chip_input}')
                    
        elif player_card_total < 22: # Player option condition player has the option to draw 1 card again or stop and end their turn
            print("You have the option to draw 1 card or stay")
          
            print("Available actions:")           
            for action in actions:
                print(action)    
            if action in actions:

                while True:
                    action = input("Do you want to draw or stay: ")
                    if action == "draw": # Player keeps drawing the cards
                        e = random.randint(1,11)
                        player_card_total += e
                        print (f'Your new card total is {player_card_total}')
                    elif action == "stay": # Player stops drawing and end turn
                        print('You stay on this round')
                        break
                    else: # If input is not one of the two options
                        print("{} is not an action you can do".format(str(action))) 
            # Game Message
            print (f'Your new card total is {player_card_total}')

# """---------------------------------------------------------------------------------------------------------------------------"""
    
        # Game Message
        print ("Dealer: 'shuffuling and handing out two cards to the computer'")

        # Computer Card draw
        c = random.randint(1,11)
        d = random.randint(1,11)
        computer_card_total = c + d
        print (f'The computer has the card total of:{computer_card_total}')
    
        # Storing the bet input for the computer
        computer = new_game()

# """---------------------------------------------------------------------------------------------------------------------------"""
        
        # Checks the Win, Lose and Draw conditions for the player
        if computer_card_total > 21: # Computers lose condition 
            print("The winner is the player")
            player_bet_input += 5
            player_chip_input += player_bet_input
            print(f'Your bet +5 chips are added to your pile')
            print(f'Your total chip pile {player_chip_input}')

        elif computer_card_total == 21: # Computers win condition
            print("The winner is the computer")
            player_chip_input -= player_bet_input
            print(f'You lost your betting chips from your pile')
            print(f'Your total chip pile {player_chip_input}')

        elif computer_card_total < 17: # Computer draw condition
            print("the computer draws a card")
            f = random.randint(1,11)
            computer_card_total += f
            print (f'The computers new card total is {computer_card_total}')

            if computer_card_total > 21: # Computers lose condition 
                print("The winner is the player")
                player_bet_input += 5
                player_chip_input += player_bet_input
                print(f'Your bet +5 chips are added to your pile')
                print(f'Your total chip pile {player_chip_input}')

            elif computer_card_total == 21: # Computers win condition
                print("The winner is the computer")
                player_chip_input -= player_bet_input
                print(f'You lost your betting chips from your pile')
                print(f'Your total chip pile {player_chip_input}')

            else: # Uncompleted 
                print ("The Round ends in a Draw")

# """---------------------------------------------------------------------------------------------------------------------------"""
             
        # Checks if the user wants to keep on playing
        answer = input("Do you want to continue playing or withdraw: ")
        if answer == "withdraw":
            print(f'Your scrore is {player_chip_input} chips')
            play = False # Ends the game