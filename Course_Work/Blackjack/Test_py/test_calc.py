# This is where I test all the calculation for the game:
# - Card add up function
# - Point add up function 
# - Point subtract function 
# - Positive number input
# - Negative number input



# This test if the input of number is a positive 
def validate_user_input_positive(n):
    return n >0
    return False

def test_validate_user_input_positive():
    assert validate_user_input_positive(200)


# This test if the unput of a number is a negative
def validate_user_input_negative(n):
    return n <0
    return False

def test_validate_user_input_negative():
    assert validate_user_input_negative(-200)

"""-----------------------------------------------------"""

# This test if the adding of cards would work 
def add(x, y):
    """Add Function"""
    return x + y

def test_add():
    assert(add(10, 5), 15)


# This test if the subtracting of chips would work 
def subtract_chip(x, y):
    """subtract Function"""
    return x - y

def test_subtract_chip():
    assert(subtract_chip(50, 5), 45)


# This test if the adding of chips would work 
def add_chip(x, y):
    """Add Function"""
    return x + y

def test_add_chip():
    assert(add_chip(50, 5), 55)




