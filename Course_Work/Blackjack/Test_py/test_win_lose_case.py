# This is where I test all the Win, Lose and Draw cases for the game:
# - player Win case
# - player Lose case
# - computer Win case
# - computer Lose case
# - game Draw case


"""-----------------------------------------------------"""

# This is the win case where if the player has 21 cards total the player wins the round
def player_win_case(n):
    return n == 21
    return False

def test_player_win_case():
    assert player_win_case(21)


# This is the lose case where if the player has 22 cards total the player loses the round
def player_lose_case(n):
    return n == 22
    return False

def test_player_lose_case():
    assert player_lose_case(22)


# This is the option case where the player has 2 options (end-round or draw-card) if they have <22
def player_option_case(n):
    return n < 22
    return False

def test_player_option_case():
    assert player_option_case(18)

"""-----------------------------------------------------"""

# This is the win case where if the computer has 21 cards total the computer wins the round
def computer_win_case(n):
    return n == 21
    return False

def test_computer_win_case():
    assert computer_win_case(21)


# This is the lose case where if the computer has >21 cards the computer loses the round
def computer_lose_case(n):
    return n > 21
    return False

def test_computer_lose_case():
    assert computer_lose_case(22)


# This is the draw card case where if the computer has the total of card to be <17 the computer is to draw a card
def computer_draw_card_case(n):
    return n < 17
    return False

def test_computer_draw_card_case():
    assert computer_draw_card_case(12)

"""-----------------------------------------------------"""

# This is the draw case if both the computer and player has the same total amount of cards
def card_total_equal_player(n):
    return n == 21
    return False
def card_total_equal_computer(n):
    return n == 21
    return False

def test_card_total_equal():
    assert card_total_equal_player(21)
    assert card_total_equal_computer(21)


# This is the draw case is both the player and computer has >21 card total

def card_total_player(n):
    return n < 21
    return False
def card_total_computer(n):
    return n < 21
    return False

def test_card_total():
    assert card_total_player(19)
    assert card_total_computer(20)